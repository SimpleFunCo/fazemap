package com.gmail.simplefunco.fazemap.Model;
/* Created on 7/10/2017. */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgeRange {

    @SerializedName("min")
    @Expose
    private Integer min;

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }
}
