package com.gmail.simplefunco.fazemap.Model;
/* Created on 7/9/2017. */

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FbUsersPojo {

    @SerializedName("data")
    private List<FbUserDataPojo> data = null;

    public List<FbUserDataPojo> getData() {
        return data;
    }

    public void setData(List<FbUserDataPojo> data) {
        this.data = data;
    }
}
