package com.gmail.simplefunco.fazemap.Ui;
/* Created on 7/9/2017. */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.gmail.simplefunco.fazemap.R;

import java.util.Arrays;


public class LogInFragment extends Fragment {

    private CallbackManager mCallbackmanager;
    // private ProfileTracker mProfileTracker;
    private Callbacks mCallbacks;

    private LoginButton loginButton;
    // private FbUsersPojo fbUsers;

    public interface Callbacks {
        void setNextFragment(Fragment fragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mCallbackmanager = CallbackManager.Factory.create();


        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "email",
                "user_friends",
                "user_location",
                "user_birthday"));
        loginButton.setFragment(this);

            loginButton.registerCallback(mCallbackmanager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    mCallbacks.setNextFragment(new UserInfoFragment());
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {
                }
            });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Profile.getCurrentProfile() != null) {
            LoginManager.getInstance().logOut();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackmanager.onActivityResult(requestCode, resultCode, data);
    }
}
