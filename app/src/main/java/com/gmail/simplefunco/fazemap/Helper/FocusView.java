package com.gmail.simplefunco.fazemap.Helper;
/* Created on 7/9/2017. */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class FocusView extends View {

    private Paint mTransparentPaint;
    private Paint mSemiBlackPaint;
    private Paint mRedPaint;
    private Path mPath = new Path();
    private Context mContext;

    public FocusView(Context context) {
        super(context);
        mContext = context;
        initPaints();
    }

    public FocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initPaints();
    }

    public FocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initPaints();
    }

    private void initPaints() {
        mTransparentPaint = new Paint();
        mTransparentPaint.setColor(Color.TRANSPARENT);
        mTransparentPaint.setStrokeWidth(10);
        mTransparentPaint.setShadowLayer(10.0f, 0.0f, 2.0f, 0xFF000000);
        mTransparentPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mSemiBlackPaint = new Paint();
        mSemiBlackPaint.setColor(Color.TRANSPARENT);
        mSemiBlackPaint.setStrokeWidth(10);
        mSemiBlackPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mRedPaint = new Paint();
        mRedPaint.setColor(Color.RED);
        mRedPaint.setStrokeWidth(35);
        mRedPaint.setStyle(Paint.Style.STROKE);
        mRedPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int smallerSize;
        if (size.x < size.y) {
            smallerSize = size.x;
        } else {
            smallerSize = size.y;
        }

        mPath.reset();
        canvas.drawCircle(canvas.getWidth() / 2,
                canvas.getHeight() / 2, (smallerSize - (16 * 4)) / 2, mRedPaint);

        mPath.addCircle(canvas.getWidth() / 2,
                canvas.getHeight() / 2, (smallerSize - (16 * 4)) / 2, Path.Direction.CW);
        mPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);

        canvas.drawCircle(canvas.getWidth() / 2,
                canvas.getHeight() / 2, (smallerSize - (16 * 4)) / 2, mTransparentPaint);

        canvas.drawPath(mPath, mSemiBlackPaint);
        canvas.clipPath(mPath);
        canvas.drawColor(Color.parseColor("#FFFFFFFF"));
    }
}
