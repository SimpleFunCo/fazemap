package com.gmail.simplefunco.fazemap.Model;
/* Created on 7/9/2017. */

import com.google.gson.annotations.SerializedName;

public class FbUserDataPojo {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("picture")
    private FbUserPictureDataPojo picture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FbUserPictureDataPojo getPicture() {
        return picture;
    }

    public void setPicture(FbUserPictureDataPojo picture) {
        this.picture = picture;
    }
}
