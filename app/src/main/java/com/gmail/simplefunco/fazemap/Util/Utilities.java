package com.gmail.simplefunco.fazemap.Util;
/* Created on 7/10/2017. */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class Utilities {

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.d(TAG, "isOnline: " + activeNetwork.getTypeName());
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                Log.d(TAG, "isOnline: " + activeNetwork.getTypeName());
                return true;
            } else {
                Log.d(TAG, "isOnline: FAILUER Somehow you get here " +
                        activeNetwork.getTypeName());
                return false;
            }
        } else {
            return false;
        }
    }
}
