package com.gmail.simplefunco.fazemap.Ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.facebook.Profile;
import com.gmail.simplefunco.fazemap.R;

public class MainActivity extends FragmentActivity
        implements LogInFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Profile.getCurrentProfile() == null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            LogInFragment logInFragment = new LogInFragment();
            transaction.add(R.id.activity_main_container, logInFragment).commit();
        } else {
            setNextFragment(new UserInfoFragment());
        }
    }

    @Override
    public void setNextFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        // UserInfoFragment mapsFragment = new UserInfoFragment();
        transaction.replace(R.id.activity_main_container, fragment).commit();
    }
}
