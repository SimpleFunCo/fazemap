package com.gmail.simplefunco.fazemap.Ui;
/* Created on 7/9/2017. */

import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.gmail.simplefunco.fazemap.Controller.RecyclerFriendsAdapter;
import com.gmail.simplefunco.fazemap.Model.FbUserDataPojo;
import com.gmail.simplefunco.fazemap.Model.FbUserMePojo;
import com.gmail.simplefunco.fazemap.Model.FbUsersPojo;
import com.gmail.simplefunco.fazemap.R;
import com.gmail.simplefunco.fazemap.Ui.LogInFragment.Callbacks;
import com.gmail.simplefunco.fazemap.Util.Utilities;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class UserInfoFragment extends Fragment implements OnMapReadyCallback {

    private GoogleApiClient mGoogleApiClient;
    private AccessTokenTracker mFbTracker;
    private FbUsersPojo mFbUsers;
    private FbUserMePojo mFbUserMe;
    private RecyclerFriendsAdapter mFriendsAdapter;
    private Callbacks mCallbacks;
    SharedPreferences prefs = null;

    private GoogleMap mGoogleMap;
    private SupportMapFragment mapFragment;
    private CircleImageView mImageViewPhoto;
    private TextView mTextViewFirstName;
    private TextView mTextViewLastName;
    private TextView mTextViewBirthDate;
    private TextView mTextViewGender;
    private RecyclerView mRecyclerViewFriends;
    private TextView mTextViewUserLocation;
    private TextView mTextViewUserAgeRange;
    private TextView mTextViewUserLink;
    private TextView mTextViewUserLocale;
    private TextView mTextViewUserVerified;
    private RelativeLayout mLayoutBlockUi;
    private ProgressBar mProgressBarLoading;
    private Button mButtonRetry;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_info_loading, container, false);

        prefs = getActivity().getSharedPreferences("com.gmail.simplefunco.fazemap", MODE_PRIVATE);

        mImageViewPhoto = (CircleImageView) view.findViewById(R.id.user_info_photo);
        mTextViewFirstName = (TextView) view.findViewById(R.id.user_info_firstName);
        mTextViewLastName = (TextView) view.findViewById(R.id.user_info_lastName);
        mTextViewBirthDate = (TextView) view.findViewById(R.id.user_info_birthDate);
        mTextViewGender = (TextView) view.findViewById(R.id.user_info_gender);
        mTextViewUserLocation = (TextView) view.findViewById(R.id.user_info_location);
        mTextViewUserAgeRange = (TextView) view.findViewById(R.id.user_info_ageRange);
        mTextViewUserLink = (TextView) view.findViewById(R.id.user_info_link);
        mTextViewUserLocale = (TextView) view.findViewById(R.id.user_info_locale);
        mTextViewUserVerified = (TextView) view.findViewById(R.id.user_info_verified);
        mRecyclerViewFriends = (RecyclerView) view.findViewById(R.id.user_info_recyclerViewFriends);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewFriends.setLayoutManager(layoutManager);
        mRecyclerViewFriends.setItemAnimator(new DefaultItemAnimator());
        mLayoutBlockUi = (RelativeLayout) view.findViewById(R.id.fragment_user_info_blockUi);
        mProgressBarLoading = (ProgressBar) view.findViewById(R.id.fragment_user_info_barLoading);
        mButtonRetry = (Button) view.findViewById(R.id.fragment_user_info_buttonRetry);
        mButtonRetry.setOnClickListener(v -> {
            if (Utilities.isOnline(getActivity())) {
                getUserInfo();
            } else {
                Toast.makeText(getActivity(), "Please, check internet state", Toast.LENGTH_LONG).show();
                mButtonRetry.setEnabled(false);

                Timer buttonTimer = new Timer();
                buttonTimer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        getActivity().runOnUiThread(() -> mButtonRetry.setEnabled(true));
                    }
                }, 5000);
            }
        });

        if (Utilities.isOnline(getActivity())) {
            getUserInfo();
        } else {
            blockUi();
        }

        mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.user_info_googleMap);

        mFbTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken2) {
                if (accessToken2 == null) {
                    if (getActivity() != null) {
                        mCallbacks.setNextFragment(new LogInFragment());
                    }
                }
            }
        };

        return view;
    }

    private void blockUi() {
        Toast.makeText(getActivity(), "Please, check internet state", Toast.LENGTH_LONG).show();
        mLayoutBlockUi.setVisibility(View.VISIBLE);
        mProgressBarLoading.setVisibility(View.VISIBLE);
        mButtonRetry.setVisibility(View.VISIBLE);
    }

    private void unblockUi() {
        mLayoutBlockUi.setVisibility(View.INVISIBLE);
        mProgressBarLoading.setVisibility(View.INVISIBLE);
        mButtonRetry.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .build();

        if (prefs.getBoolean("firstrun", true)) {
            new ShowcaseView.Builder(getActivity())
                    .setTarget(new ViewTarget(mImageViewPhoto.getId(), getActivity()))
                    .setContentTitle("Swipe up")
                    .setContentText("To see additional information")
                    .hideOnTouchOutside()
                    // .replaceEndButton()
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .build();

            prefs.edit().putBoolean("firstrun", false).apply();
        }
    }

    private void getUserInfo() {
        unblockUi();
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                (object, response) -> {
                    JSONObject jsonObject = response.getJSONObject();
                    mFbUserMe = new Gson().fromJson(jsonObject.toString(), FbUserMePojo.class);

                    fillBottomSheet();

                    if (mapFragment != null) {
                        mapFragment.getMapAsync(UserInfoFragment.this);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,cover,name,first_name,last_name,age_range," +
                "link,gender,locale,picture,timezone,updated_time,verified,location,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getFriends() {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + Profile.getCurrentProfile().getId() + "/taggable_friends",
                null,
                HttpMethod.GET,
                response -> {
                    JSONObject jsonObject = response.getJSONObject();
                    if (jsonObject != null) {
                        mFbUsers = new Gson().fromJson(jsonObject.toString(), FbUsersPojo.class);
                        mFriendsAdapter = new RecyclerFriendsAdapter(mFbUsers, getActivity());
                    } else {
                        FbUserDataPojo emptyUser = new FbUserDataPojo();
                        emptyUser.setName("If you were an Admin of this app - you could " +
                                "see your friends here. (But you're not) Thanks FB ^_^");
                        ArrayList<FbUserDataPojo> emptyUsersList = new ArrayList<>();
                        emptyUsersList.add(emptyUser);
                        mFbUsers = new FbUsersPojo();
                        mFbUsers.setData(emptyUsersList);
                        mFriendsAdapter = new RecyclerFriendsAdapter(mFbUsers, getActivity());
                    }
                    mRecyclerViewFriends.setAdapter(mFriendsAdapter);
                    mFriendsAdapter.notifyDataSetChanged();
                }
        ).executeAsync();
    }

    private void fillBottomSheet() {
        Picasso.with(getActivity()).load(mFbUserMe.getPicture().getData().getUrl()).into(mImageViewPhoto);
        if (mFbUserMe.getFirstName() != null) {
            mTextViewFirstName.setText(mFbUserMe.getFirstName());
        } else {
            mTextViewFirstName.setText(R.string.not_filled);
        }
        if (mFbUserMe.getLastName() != null) {
            mTextViewLastName.setText(mFbUserMe.getLastName());
        } else {
            mTextViewLastName.setText(R.string.not_filled);
        }
        if (mFbUserMe.getBirthday() != null) {
            mTextViewBirthDate.setText(mFbUserMe.getBirthday());
        } else {
            mTextViewBirthDate.setText(R.string.not_filled);
        }
        if (mFbUserMe.getGender() != null) {
            mTextViewGender.setText(mFbUserMe.getGender());
        } else {
            mTextViewGender.setText(R.string.not_filled);
        }
        if (mFbUserMe.getLocation() != null) {
            if (mFbUserMe.getLocation().getName() != null) {
                mTextViewUserLocation.setText(mFbUserMe.getLocation().getName());
            } else {
                mTextViewUserLocation.setText(R.string.not_filled);
            }
        } else {
            mTextViewUserLocation.setText(R.string.not_filled);
        }
        if (mFbUserMe.getAgeRange() != null) {
            if (mFbUserMe.getAgeRange().getMin() != null)
            mTextViewUserAgeRange.setText(mFbUserMe.getAgeRange().getMin().toString());
        } else {
            mTextViewUserAgeRange.setText(R.string.not_filled);
        }
        if (mFbUserMe.getLink() != null) {
            mTextViewUserLink.setText(mFbUserMe.getLink());
        } else {
            mTextViewUserLink.setText(R.string.not_filled);
        }
        if (mFbUserMe.getLocale() != null) {
            mTextViewUserLocale.setText(mFbUserMe.getLocale());
        } else {
            mTextViewUserLocale.setText(R.string.not_filled);
        }
        if (mFbUserMe.getVerified() != null) {
            mTextViewUserVerified.setText(mFbUserMe.getVerified().toString());
        } else {
            mTextViewUserVerified.setText(R.string.not_filled);
        }

        getFriends();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng userCoordinates;
        mGoogleMap = googleMap;
        String userMeCity;

        Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());

        if (mFbUserMe.getLocation() != null) {
            userMeCity = mFbUserMe.getLocation().getName();
        } else {
            userMeCity = "Kiev";
        }
        List<Address> addresses = new ArrayList<>();

        try {
            addresses = gcd.getFromLocationName(userMeCity, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        userCoordinates =
                new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userCoordinates, 7));
        addMarker(userCoordinates, userMeCity);
    }

    private void addMarker(LatLng position, String title) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.map_48);

        mGoogleMap.addMarker(new MarkerOptions()
                .position(position)
                .icon(icon)
                .title(title));
    }
}
