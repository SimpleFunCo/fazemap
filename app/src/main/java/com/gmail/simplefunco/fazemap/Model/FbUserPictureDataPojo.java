package com.gmail.simplefunco.fazemap.Model;
/* Created on 7/9/2017. */

import com.google.gson.annotations.SerializedName;

public class FbUserPictureDataPojo {

    @SerializedName("data")
    private FbUserPicturePojo data;

    public FbUserPicturePojo getData() {
        return data;
    }

    public void setData(FbUserPicturePojo data) {
        this.data = data;
    }
}
