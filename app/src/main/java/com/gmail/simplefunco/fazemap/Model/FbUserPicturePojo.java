package com.gmail.simplefunco.fazemap.Model;
/* Created on 7/9/2017. */

import com.google.gson.annotations.SerializedName;

public class FbUserPicturePojo {

    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
