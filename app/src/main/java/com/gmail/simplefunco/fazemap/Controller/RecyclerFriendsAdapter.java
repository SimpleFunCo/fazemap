package com.gmail.simplefunco.fazemap.Controller;
/* Created on 7/10/2017. */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.simplefunco.fazemap.Model.FbUserDataPojo;
import com.gmail.simplefunco.fazemap.Model.FbUsersPojo;
import com.gmail.simplefunco.fazemap.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerFriendsAdapter extends
        RecyclerView.Adapter<RecyclerFriendsAdapter.FriendsViewHolder> {

    private FbUsersPojo mFbUsers;
    private Context mContext;

    public RecyclerFriendsAdapter(FbUsersPojo fbUsers, Context context) {
        mFbUsers = fbUsers;
        mContext = context;
    }

    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_friends, parent, false);

        return new FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendsViewHolder holder, int position) {
        FbUserDataPojo fbUser = mFbUsers.getData().get(position);

        if (fbUser.getPicture() != null) {
            Picasso.with(mContext).load(fbUser.getPicture().getData().getUrl())
                    .into(holder.mCircleViewPhoto);
        }

        if (getItemCount() > 1) {
            holder.mTextViewName.setText(fbUser.getName());
        } else {
            holder.mTextViewName.setTextSize(16f);           // It seems that you've only one friend ;(
            holder.mTextViewName.setText(fbUser.getName());  // or just you're not admin of this app
            Picasso.with(mContext).load(R.drawable.ic_emoticon_sad_black_48dp)
                    .into(holder.mCircleViewPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return mFbUsers.getData().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView mCircleViewPhoto;
        public TextView mTextViewName;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            mCircleViewPhoto = (CircleImageView) itemView.findViewById(R.id.item_friendPhoto);
            mTextViewName = (TextView) itemView.findViewById(R.id.item_friendName);
        }
    }
}
